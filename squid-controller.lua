-- the squid  controller

local mymodule = {}

mymodule.default_action = "status"

mymodule.status = function( self )
	return self.model.getstatus()
end

mymodule.startstop = function( self )
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

mymodule.config = function( self )
	return self.handle_form(self, self.model.read_config, self.model.update_config, self.clientdata, "Save", "Edit Config", "Configuration Set")
end

mymodule.digest = function( self )
	return self.handle_form(self, self.model.read_digest_userlist, self.model.update_digest_userlist, self.clientdata, "Save", "Edit User List", "User List Set")
end

mymodule.enabledigestlist = function( self )
	return self.handle_form(self, self.model.get_enable_digest_userlist, self.model.enable_digest_userlist, self.clientdata, "Enable", "Enable Digest User List", "Digest User List enabled")
end

mymodule.expert = function( self )
	return self.handle_form(self, self.model.get_configfile, self.model.update_configfile, self.clientdata, "Save", "Edit Config", "Configuration Set")
end

function mymodule.listfiles(self)
	return self.model.listfiles(self)
end

function mymodule.createfile(self)
	return self.handle_form(self, self.model.getnewfile, self.model.createfile, self.clientdata, "Create", "Create New Squid File", "Squid File Created")
end

function mymodule.editfile(self)
	return self.handle_form(self, function() return self.model.readfile(self.clientdata.filename) end, self.model.updatefile, self.clientdata, "Save", "Edit Squid File", "Squid File Saved" )
end

function mymodule.deletefile(self)
	return self.handle_form(self, self.model.getdeletefile, self.model.deletefile, self.clientdata, "Delete", "Delete Squid File", "Squid File Deleted")
end

return mymodule
