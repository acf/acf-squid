<% local form, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<% htmlviewfunctions.displaycommandresults({"enabledigestlist"}, session) %>

<%
local header_level = htmlviewfunctions.displaysectionstart(cfe({label="Digest User List Status"}), page_info)
htmlviewfunctions.displayitem(form.value.status)
if form.value.status.errtxt then
	htmlviewfunctions.displayitem(cfe({type="form", value={}, label="", option="Enable", action="enabledigestlist" }), page_info, 0)
end
htmlviewfunctions.displaysectionend(header_level)
%>

<%
local pattern = string.gsub(page_info.prefix..page_info.controller, "[%(%)%.%%%+%-%*%?%[%]%^%$]", "%%%1")
local func = haserl.loadfile(page_info.viewfile:gsub(pattern..".*$", "/") .. "filedetails-html.lsp")
func(form, viewlibrary, page_info, session)
%>
